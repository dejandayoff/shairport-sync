FROM mikebrady/shairport-sync:classic

RUN apk add mosquitto-clients

COPY ./shairport-sync.conf /etc/shairport-sync.conf

COPY ./session_end.sh /session_end.sh
RUN chmod +x /session_end.sh

COPY ./session_start.sh /session_start.sh
RUN chmod +x /session_start.sh

# Add run script that will start SPS
COPY ./run.sh ./run.sh
RUN chmod +x /run.sh

ENTRYPOINT ["/init","./run.sh"]