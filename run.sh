#!/bin/sh

GREEN='\033[0;32m'
NC='\033[0m' # No Color

topic="audio/available-audio-server"

echo "not_active" > /status

NAME=$(sed -n '/general/,/}/{/name/ {s/.*= *"\([^"]*\)".*/\1/; p; q;}}' /etc/shairport-sync.conf)

MQTT_HOST=$(sed -n '/mqtt/,/}/{/hostname/ {s/.*= *"\([^"]*\)".*/\1/; p; q;}}' /etc/shairport-sync.conf)

while [ ! -f /var/run/avahi-daemon/pid ]; do
  echo "Warning: avahi is not running, sleeping for 1 second before trying to start shairport-sync"
  sleep 1
done

echo $NAME > /name
echo $MQTT_HOST > /mqtt_host

while true 
do
    echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Obtaining Pulse Server"
    echo "mosquitto_sub -h $MQTT_HOST -t $topic --will-topic 'audio/input/airplay/$NAME/connection' --will-payload 'disconnected'"
    mosquitto_sub -h $MQTT_HOST -t $topic --will-topic "audio/input/airplay/$NAME/connection" --will-payload "disconnected" | while read -r PAYLOAD
        do
            STATUS=$(cat /status)
            if [ "$STATUS" == "not_active" ]; then
                if [ "$PAYLOAD" != "None" ]; then
                    if [ "$PAYLOAD" != "$PULSE_SERVER" ]; then
                        if [ -n "$pid" ]; then
                            echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   new PA Server, exiting"
                            exit 0
                        fi

                        export PULSE_SERVER=$PAYLOAD
                        echo $PULSE_SERVER > /pa_server
                        echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Connecting to PulseAudio server at: $PULSE_SERVER."

                        mosquitto_pub -h $(cat /mqtt_host) -t "audio/input/airplay/$(cat /name)/pa_server" -m "$(cat /pa_server)"

                        if ! mkdir /tmp/shairport.lock; then
                            printf "Failed to acquire lock.\n" >&2
                            exit 1
                        fi
                        trap 'rm -rf /tmp/shairport.lock' EXIT  # remove the lockdir on exit

                        /usr/local/bin/shairport-sync "$@"
                    fi
                else
                    if [ ! -d "/tmp/shairport.lock" ]; then 
                        echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   No available PulseAudio servers. Shairport did not start."
                    fi
                fi
            else
                echo -e "[$(date -u +'%Y-%m-%dT%H:%M:%SZ') ${GREEN}INFO${NC}  wrapper]   Shairport is active, not changing anything."
            fi
        done
done
