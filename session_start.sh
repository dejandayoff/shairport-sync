#/bin/sh

mosquitto_pub -h $(cat /mqtt_host) -t "audio/input/airplay/$(cat /name)/pa_server" -m "$(cat /pa_server)"

echo "active" > /status